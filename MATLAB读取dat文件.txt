function [SIgnal, Isvalid_label, Subject_label] = readDATfile(file)
%% 读取脑电信号文件.eeg转换为.dat文件的函数，一个.dat文件就是包含同一个受试者的N次实验的n=32根电极的m=1000采样率的脑电信号
%%.dat文件格式如下：
% [Epoch Header]
% [Trial Type]	2
% [Accept]	1
% [Correct]	0
% [RT]	 0.000
% [Resp]	0
% [4.39190000000000,9.09760000000000,3.19980000000000,4.07820000000000,4.83110000000000,5.45850000000000,4.07820000000000,8.15640000000000,0.376500000000000,0.564700000000000,5.52130000000000,4.83110000000000,6.52510000000000,5.20760000000000,0.501900000000000,4.14100000000000,6.65060000000000,5.45850000000000,0.627400000000000,4.64290000000000,4.51740000000000,5.33300000000000,5.58400000000000,2.94890000000000,1.94500000000000,1.19210000000000,2.94890000000000,4.32920000000000,2.94890000000000,-1.75680000000000,-1.81950000000000,-1.56850000000000]
% ...
% ...
%% PS: 上面所示数据是m*n列数据，m是采样率，n是电极数，这里取值m=1000HZ，n=32
%% Output:
%% SIgnal：如果做了N次实验，则为1*N的元胞数组，每个元胞是n行m列数组
%% Isvalid_label：如果做了N次实验，则为1*N的数值数组：若第x个数值为1，表明第x次实验有效；若第y个数值为0，表明第y次实验无效；
%% Subject_label：受试者所聚焦的对象标签
    fid = fopen(file);
    i = 1;
    while ~feof(fid)
        fgetl(fid);%读[Epoch Header]，没用到
        s{i} = fgetl(fid);%读[Trial Type]	2
        b{i} = fgetl(fid);%读[Accept]	1
        fgetl(fid);%读[Correct]	0，没用到
        fgetl(fid);%读[RT]	 0.000，没用到
        fgetl(fid);%读[Resp]	0，没用到
        SIgnal{i} = fscanf(fid, '%f', [32, inf]);%读 1*n 行数据，读的时候从dat/txt等文件中按行读，但存的时候往矩阵中按列存
        i=i+1;%读取下一个[Epoch Header]，从而循环
    end
    fclose('all');
    
    Num = length(SIgnal);%实验的次数，即脑电信号的个数
    for i = 1:Num
        temk = b{i};
        teml = s{i};
        ts=temk(regexp(temk, '\d'));%取出文本中的数字
        tsd=teml(regexp(teml, '\d'));%取出文本中的数字
        Isvalid_label(i) = str2double(ts(1));
        Subject_label(i) = str2double(tsd);
    end
end